#pragma once
#ifndef TCP_SERVER_H_
#define TCP_SERVER_H_

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "ws2812b_writer.h"

portTASK_FUNCTION_PROTO(tcp_server, params);

#endif /* TCP_SERVER_H_ */
