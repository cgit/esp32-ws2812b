// List of parameters.
//          type       name         display name,    default value

STATE_PARAM(uint32_t,  time,        "Current Time",  0)
STATE_PARAM(int,       timetick,    "Speed",         100)      
STATE_PARAM(uint8_t,   brightness,  "Brightness",    50)       
STATE_PARAM(uint8_t,   n_snow,      "Snow Effect",   10)
STATE_PARAM(uint8_t,   n_red,       "Desaturation",  32)       
STATE_PARAM(int,       x_scale,     "X Scale",       255)       
STATE_PARAM(bool,      power,       "Power",         true)     
STATE_PARAM(bool,      cool,        "Cool",          false)    
STATE_PARAM(bool,      invert,      "Invert",        false)    
