#pragma once
#ifndef INCLUDE_DRV_WS2812B_H_
#define INCLUDE_DRV_WS2812B_H_

#include <stdint.h>

#include "driver/spi_master.h"

typedef struct {
  uint8_t r;
  uint8_t g;
  uint8_t b;
} ws2812b_rgb_t;

typedef struct {
  /* Double-buffering strategy for speed-up. */
  uint8_t* buf_1;
  uint8_t* buf_2;
  uint8_t* buf_; /* Points to either buf_1 or buf_2 */

  uint32_t n_rgb;      /* Number of rgb values which exist. */
  ws2812b_rgb_t rgb[]; /* Colors to write. */
} ws2812b_buffer_t;

struct WS2812B;
typedef struct WS2812B ws2812b_t;

ws2812b_buffer_t* ws2812b_new_buffer(uint32_t size);

esp_err_t ws2812b_wait(ws2812b_t* drv);

ws2812b_t* ws2812b_init(spi_device_handle_t spi);

esp_err_t ws2812b_write(ws2812b_t* drv, ws2812b_buffer_t* buffer);

static inline void ws2812b_buffer_set_rgb(
    ws2812b_buffer_t* buf, size_t idx, uint8_t r, uint8_t g, uint8_t b)
{
  if (idx >= buf->n_rgb) {
    return;
  }

  buf->rgb[idx].r = r;
  buf->rgb[idx].g = g;
  buf->rgb[idx].b = b;
}

static inline void ws2812b_buffer_set_color(
    ws2812b_buffer_t* buf, size_t idx, ws2812b_rgb_t* color)
{
  if (idx >= buf->n_rgb) {
    return;
  }

  buf->rgb[idx] = *color;
}

#endif /* INCLUDE_DRV_WS2812B_H_ */
