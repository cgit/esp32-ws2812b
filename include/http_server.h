#pragma once
#ifndef HTTP_SERVER_H_
#define HTTP_SERVER_H_

#include <esp_http_server.h>
#include <esp_log.h>

#include "ws2812b_writer.h"

httpd_handle_t start_webserver(ws_params_t* params);

#endif /* HTTP_SERVER_H_ */
