#pragma once
#ifndef WS2812B_WRITER_H_
#define WS2812B_WRITER_H_

#include "drv/ws2812b.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

extern const int NUMBER_PARAMS;

typedef struct {
  ws2812b_t* drv;

  struct {
#define STATE_PARAM(t, n, ...) t n;
#include "state_params.i"
#undef STATE_PARAM
  } state;
} ws_params_t;

void reset_parameters(ws_params_t* params);

portTASK_FUNCTION_PROTO(ws2812b_write_task, params);

#endif /* WS2812B_WRITER_H_ */
