#pragma once
#ifndef STATION_H_
#define STATION_H_

void wifi_init_station(const char* ssid, const char* psk);

#endif /* STATION_H_ */
