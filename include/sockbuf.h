#pragma once
#ifndef SOCKBUF_H_
#define SOCKBUF_H_

#include <inttypes.h>

#define SOCKBUF_BUFLEN 128
#define SOCKBUF_CLOSED 1

typedef struct {
  uint8_t buf[SOCKBUF_BUFLEN];

  uint8_t next;
  uint8_t last;

  int socket;
  uint8_t flags;
} sockbuf_t;

/* Initialize a sockbuf from the provided file descriptor. */
sockbuf_t* init_sockbuf(sockbuf_t* sockbuf, int socket);

/* Writes a null-terminated string to the socket. */
void sockbuf_write(sockbuf_t* sockbuf, const char* value);

/*
 * Return the next character of the sockbuf or -1 if EOF was reached.
 *
 * This does not advance the pointer.
 */
int peek_char(sockbuf_t* sockbuf);

/*
 * Return and consume the next character of the sockbuf or -1 if EOF was
 * reached.
 */
int get_char(sockbuf_t* sockbuf);

#endif /* SOCKBUF_H_ */
