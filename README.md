# Hacking on an Espressif ESP32.

The goal of this project is to get some workable Christmas lights using a spool
of ws2812b LED strip lights. I've done this before, bare metal, on an stm32
(see git.josher.dev:stm32.git). I expect this to be not _too_ difficult
considering I have done it before on bare metal.

A stretch goal is to make these controllable over the network, and since the
esp32 comes with wireless already on the board, this should not be too
difficult.

Unfortunately, I am not a huge fan of the Espressif idf infrastructure. It seems
_very_ bloated. Plus it uses CMake, which I don't have anything against, I just
don't know it very well. Hopefully I'll learn it better!

# Building and Flashing

To build, first you have to install the esp32-idf (ugh, I know right, bunch of
bloat. Maybe sometime soon I'll start bare-metal hacking on it.)

Once you've followed the instructions there, it's time to build.

```bash
idf.py build
```

```bash
idf.py -p /dev/ttyUSB0 flash
```
