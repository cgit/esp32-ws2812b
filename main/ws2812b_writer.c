#include "ws2812b_writer.h"

#include <stdlib.h>
#include <string.h>

const int NUMBER_PARAMS =
#define STATE_PARAM(...) +1
#include "state_params.i"
#undef STATE_PARAM
    ;

#define SIZE 300

/* Returns round(sin(2πn / 256) * 127.5 + 127.5). */
uint8_t byte_sin(uint8_t n);

static inline uint8_t byte_scale(uint8_t n, uint8_t sc)
{
  return n * sc / 255;
}

#define min(a, b) (a) < (b) ? (a) : (b)
#define max(a, b) (a) > (b) ? (a) : (b)

/* returns (in / 256)^n * 256. */
uint8_t amp(uint8_t in, uint8_t n);

void reset_parameters(ws_params_t* params)
{
  memset(&params->state, 0, sizeof(params->state));

#define STATE_PARAM(type, attr, displ, def, ...) params->state.attr = def;
#include "state_params.i"
#undef STATE_PARAM
}

void calculate_colors(ws_params_t* ws_params, ws2812b_buffer_t* buffer)
{
  uint32_t red = 0, green = 0, blue = 0;
  int time;

  for (int i = 0; i < SIZE; ++i) {
    int x = i * 255 / ws_params->state.x_scale;
    time = ws_params->state.time;

    if (!ws_params->state.power) {
      ws2812b_buffer_set_rgb(buffer, i, 0, 0, 0);
      continue;
    }

    red = byte_scale(
              byte_sin(time / 1000 + x * 4), 255 - ws_params->state.n_red) +
          ws_params->state.n_red;
    green = 255 - red;

    if (ws_params->state.cool) {
      uint32_t tmp = green;
      green = blue;
      blue = tmp;
    }

    /* Add a little white flair that comes around every once in a while. */

    uint32_t whitesum = 0;
    if (ws_params->state.n_snow) {
      uint32_t white[] = {
          /* Parallax "snow" */
          time / 179 + x * 8,
          time / 193 + x * 12,
          time / 211 + x * 16,
          time / 233 + x * 8,
          // (ws_params->state.time + 128) / 233 + x * 8,
      };

      for (int i = 0; i < sizeof(white) / sizeof(uint32_t); ++i) {
        if ((white[i] / 256) % ws_params->state.n_snow == 0) {
          white[i] = amp(byte_sin(white[i]), 20);
        } else {
          white[i] = 0;
        }
        whitesum += white[i];
      }
    }

    uint8_t brightness = ws_params->state.brightness;
    red = min(red + whitesum, 255);
    green = min(green + whitesum, 255);
    blue = min(blue + whitesum, 255);

    if (ws_params->state.invert) {
      red = 255 - red;
      green = 255 - green;
      blue = 255 - blue;
    }

    red = byte_scale(red, brightness);
    green = byte_scale(green, brightness);
    blue = byte_scale(blue, brightness);

    ws2812b_buffer_set_rgb(buffer, i, red, green, blue);
  }
}

portTASK_FUNCTION(ws2812b_write_task, params)
{
  ws_params_t* ws_params = (ws_params_t*)params;
  reset_parameters(ws_params);

  ws2812b_buffer_t* buffer = ws2812b_new_buffer(SIZE);

  for (;;) {
    // Copy the parameters to avoid a change occuring during the calculation.
    ws_params_t copy_params;
    memcpy(&copy_params, ws_params, sizeof(ws_params_t));

    calculate_colors(&copy_params, buffer);
    ws2812b_write(copy_params.drv, buffer);
    vTaskDelay(1);
  }
}

uint8_t sintable[256] = {
    128, 131, 134, 137, 140, 143, 146, 149, 152, 155, 158, 162, 165, 167, 170,
    173, 176, 179, 182, 185, 188, 190, 193, 196, 198, 201, 203, 206, 208, 211,
    213, 215, 218, 220, 222, 224, 226, 228, 230, 232, 234, 235, 237, 238, 240,
    241, 243, 244, 245, 246, 248, 249, 250, 250, 251, 252, 253, 253, 254, 254,
    254, 255, 255, 255, 255, 255, 255, 255, 254, 254, 254, 253, 253, 252, 251,
    250, 250, 249, 248, 246, 245, 244, 243, 241, 240, 238, 237, 235, 234, 232,
    230, 228, 226, 224, 222, 220, 218, 215, 213, 211, 208, 206, 203, 201, 198,
    196, 193, 190, 188, 185, 182, 179, 176, 173, 170, 167, 165, 162, 158, 155,
    152, 149, 146, 143, 140, 137, 134, 131, 128, 124, 121, 118, 115, 112, 109,
    106, 103, 100, 97,  93,  90,  88,  85,  82,  79,  76,  73,  70,  67,  65,
    62,  59,  57,  54,  52,  49,  47,  44,  42,  40,  37,  35,  33,  31,  29,
    27,  25,  23,  21,  20,  18,  17,  15,  14,  12,  11,  10,  9,   7,   6,
    5,   5,   4,   3,   2,   2,   1,   1,   1,   0,   0,   0,   0,   0,   0,
    0,   1,   1,   1,   2,   2,   3,   4,   5,   5,   6,   7,   9,   10,  11,
    12,  14,  15,  17,  18,  20,  21,  23,  25,  27,  29,  31,  33,  35,  37,
    40,  42,  44,  47,  49,  52,  54,  57,  59,  62,  65,  67,  70,  73,  76,
    79,  82,  85,  88,  90,  93,  97,  100, 103, 106, 109, 112, 115, 118, 121,
    124,
};

uint8_t byte_sin(uint8_t n)
{
  return sintable[n];
}

uint8_t amp(uint8_t in, uint8_t n)
{
  uint32_t out = in;

  for (int i = 0; i < n; ++i) {
    out = (out * in) / 256;
  }

  return min(out, 255);
}
