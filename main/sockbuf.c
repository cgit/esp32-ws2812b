#include "sockbuf.h"

#include <string.h>
#include <unistd.h>

void sockbuf_write(sockbuf_t* sockbuf, const char* value)
{
  write(sockbuf->socket, value, strlen(value));
}

sockbuf_t* init_sockbuf(sockbuf_t* sockbuf, int socket)
{
  memset(sockbuf, 0, sizeof(sockbuf_t));
  sockbuf->socket = socket;

  return sockbuf;
}

int peek_char(sockbuf_t* sockbuf)
{
  if (sockbuf->next == sockbuf->last) {
    sockbuf->next = 0;
    int size = read(sockbuf->socket, sockbuf->buf, sizeof(sockbuf->buf));
    sockbuf->last = (uint8_t)size;

    if (size <= 0) {
      return size - 1;
    }
  }

  return sockbuf->buf[sockbuf->next];
}

int get_char(sockbuf_t* sockbuf)
{
  int ret = peek_char(sockbuf);

  if (ret >= 0) {
    sockbuf->next++;
  }

  return ret;
}
