#include "driver/spi_master.h"
#include "drv/ws2812b.h"
#include "esp_spi_flash.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "http_server.h"
#include "sdkconfig.h"
#include "station.h"
#include "tcp_server.h"
#include "ws2812b_writer.h"

#ifdef CONFIG_IDF_TARGET_ESP32
#define LCD_HOST HSPI_HOST

#define PIN_NUM_MISO 25
#define PIN_NUM_MOSI 23
#define PIN_NUM_CLK 19
#define PIN_NUM_CS 22

#define PIN_NUM_DC 21
#define PIN_NUM_RST 18
#define PIN_NUM_BCKL 5
#elif defined CONFIG_IDF_TARGET_ESP32S2
#define LCD_HOST SPI2_HOST

#define PIN_NUM_MISO 37
#define PIN_NUM_MOSI 35
#define PIN_NUM_CLK 36
#define PIN_NUM_CS 34

#define PIN_NUM_DC 4
#define PIN_NUM_RST 5
#define PIN_NUM_BCKL 6
#elif defined CONFIG_IDF_TARGET_ESP32C3
#define LCD_HOST SPI2_HOST

#define PIN_NUM_MISO 2
#define PIN_NUM_MOSI 7
#define PIN_NUM_CLK 6
#define PIN_NUM_CS 10

#define PIN_NUM_DC 9
#define PIN_NUM_RST 4
#define PIN_NUM_BCKL 5
#endif

static ws_params_t ws_params;

/* Ticks the clock at a constant rate independent of the time it takes to write
 * to the led strip. */
portTASK_FUNCTION(time_keeper, params_)
{
  ws_params_t* params = (ws_params_t*)params_;

  TickType_t last_wake;
  const TickType_t freq = 1;

  // Initialise the xLastWakeTime variable with the current time.
  last_wake = xTaskGetTickCount();

  for (;;) {
    // Wait for the next cycle.
    vTaskDelayUntil(&last_wake, freq);
    params->state.time += params->state.timetick;
  }
}

void app_main(void)
{
  esp_err_t error;
  spi_device_handle_t spi;
  spi_bus_config_t cfg = {
      .miso_io_num = PIN_NUM_MISO,
      .mosi_io_num = PIN_NUM_MOSI,
      .sclk_io_num = PIN_NUM_CLK,
      .quadwp_io_num = -1,
      .quadhd_io_num = -1,
      .max_transfer_sz = 320 * 2 + 8,
  };

  spi_device_interface_config_t devcfg = {
      .clock_speed_hz = 24 * 100 * 1000, /* 2.5 MHz */
      .mode = 0,
      .spics_io_num = PIN_NUM_CS,
      .queue_size = 7,
      .pre_cb = NULL,
  };

  printf("Hello, World!\n");
  printf("miso: %d\n", PIN_NUM_MISO);
  printf("mosi: %d\n", PIN_NUM_MOSI);
  printf("sclk: %d\n", PIN_NUM_CLK);
  printf("Tick period ms: %d\n", (int)portTICK_PERIOD_MS);

  error = spi_bus_initialize(HSPI_HOST, &cfg, SPI_DMA_CH_AUTO);
  printf("Bus Init\n");
  ESP_ERROR_CHECK(error);

  error = spi_bus_add_device(HSPI_HOST, &devcfg, &spi);
  printf("Add Device\n");
  ESP_ERROR_CHECK(error);

  ws_params.drv = ws2812b_init(spi);

  printf("Configuration complete!!\n");

  ESP_LOGI("main", "Connection to AP");
  wifi_init_station("Wort", "JoshIsBau5");
  ESP_LOGI("main", "Complete!");

  xTaskCreate(time_keeper, "time_keeper", 1024, &ws_params, 1, NULL);
  xTaskCreate(ws2812b_write_task, "ws2812b_writer", 4096, &ws_params, 1, NULL);
  xTaskCreate(tcp_server, "tcp_server", 4096, &ws_params, 2, NULL);

  start_webserver(&ws_params);
}
